﻿using DocumentViewer.Domain.Documents;
using DocumentViewer.Infrastructure.Context;
using DocumentViewer.Infrastructure.QueryHandlers;
using DocumentViewer.UnitTests.Common;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace DocumentViewer.UnitTests.QueryHandlers
{
    public class DocumentQueryHandlerTest
    {
        [Fact]
        public void ShouldReturnEmptyByte_If_FiltersDoesNotMatchToAny()
        {
            var list = new List<DocumentEntity>();
            var documentsMock = EFMocks.CreateDbSetMock(list);
            var dbContextMock = new Mock<IDocumentViewerDbContext>();
            dbContextMock.Setup(x => x.Documents).Returns(documentsMock.Object);

            var handler = new DocumentsQueryHandler(dbContextMock.Object);

            var result = handler.Handle(new GetDocument(DateTime.MinValue, "test", 1)).GetAwaiter().GetResult();

            Assert.Empty(result.Document);
        }

        [Fact]
        public void ShouldReturn_NotEmptyByteDocument()
        {
            var list = this.GetSomeTestData();
            var documentsMock = EFMocks.CreateDbSetMock(list);
            var dbContextMock = new Mock<IDocumentViewerDbContext>();
            dbContextMock.Setup(x => x.Documents).Returns(documentsMock.Object);

            var handler = new DocumentsQueryHandler(dbContextMock.Object);

            var result = handler.Handle(new GetDocument(null, "test", null)).GetAwaiter().GetResult();

            Assert.NotEmpty(result.Document);
        }

        private List<DocumentEntity> GetSomeTestData()
        {
            byte[] array = new byte[8]; 
            Random random = new Random();
            random.NextBytes(array);

            var list = new List<DocumentEntity>();
            list.Add(new DocumentEntity("test", array, DateTime.MinValue, 12));
            list.Add(new DocumentEntity("documentA", new byte[0], DateTime.MaxValue, 11));

            return list;
        }

    }
}
