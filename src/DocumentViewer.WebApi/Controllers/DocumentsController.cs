﻿using DocumentViewer.Docuvieware3;
using DocumentViewer.Domain.Documents;
using DocumentViewer.Domain.Queries;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace DocumentViewer.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class DocumentsController : Controller
    {
        private readonly IQueryBus _queryBus;
        private readonly IDocuViewareConfigurationFactory _docuViewareConfigurationFactory;
        private readonly DocuviewareService _docuviewareService;
        public DocumentsController(IQueryBus queryBus, 
            IDocuViewareConfigurationFactory docuViewareConfigurationFactory,
            DocuviewareService docuviewareService)
        {
            _queryBus = queryBus;
            _docuViewareConfigurationFactory = docuViewareConfigurationFactory;
            _docuviewareService = docuviewareService;
        }

        [HttpGet("[action]")]
        public async Task<DocuViewareRESTOutputResponse> Get([FromQuery] DocumentQueryItem item)
        {
            var document = await _queryBus.Send<GetDocument, DocumentItem>(
                new GetDocument(item.Date, item.DocumentName, item.DocumentNumber));

            var config = _docuViewareConfigurationFactory.GetConfiguration(document.Document, item.SessionId);

            var markup = await _docuviewareService.GetDocument(config);

            return markup;
        }
    }
}
