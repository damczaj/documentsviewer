import React, { Component } from 'react';
import { FilterableDocumentsViewer } from './components/Documents/FilterableDocumentsViewer';

export default class App extends Component {
  displayName = App.name

  render() {
    return (
        <FilterableDocumentsViewer />
    );
  }
}
