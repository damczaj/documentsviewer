import React, { Component } from 'react';
import $ from 'jquery';

export class DocumentsViewer extends Component {


  constructor(props) {
    super(props);
    this.myRef = React.createRef();
    this.state = { width: 0, height: 0 };
    this.appendHTML = this.appendHTML.bind(this);
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
  }

  appendHTML(html) {
    $(this.myRef.current).html(html);
  }

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }

  updateWindowDimensions() {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
  }

  render() {
    const error = this.props.fetchError
    if (error) {
      return (
        <h4> Unable to get document </h4>
      );
    }
    
    const style = {
      width: this.state.width - 25,
      height: this.state.height - 35
    }
    return (
      <div style={style} ref={this.myRef} />
    );
  }
}
