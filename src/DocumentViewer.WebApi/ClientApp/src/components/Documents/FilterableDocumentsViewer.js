import React, { Component } from 'react';
import { DocumentsSearchBar } from './DocumentsSearchBar';
import { DocumentsViewer } from './DocumentsViewer';

export class FilterableDocumentsViewer extends Component {

  constructor(props) {
    super(props);

    this.myRef = React.createRef();

    this.state = { fetchError: false, filter: { documentName: '', documentNumber: '', date: null } };

    this.handleDocumentNameChange = this.handleDocumentNameChange.bind(this);
    this.handleDocumentNumberChange = this.handleDocumentNumberChange.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.handleSearchButtonClick = this.handleSearchButtonClick.bind(this);
  }

  componentDidMount() {
    this.getFiltered();
  }

  handleDocumentNameChange(docName) {
    this.setState((state, props) => ({
      filter: { documentName: docName, documentNumber: state.filter.documentNumber, date: state.filter.date }
    }));
  }

  handleDocumentNumberChange(docNumber) {
    this.setState((state, props) => ({
      filter: { documentName: this.state.filter.documentName, documentNumber: docNumber, date: state.filter.date }
    }));
  }

  handleDateChange(date) {
    this.setState((state, props) => ({
      filter: { documentName: this.state.filter.documentName, documentNumber: state.filter.documentNumber, date: date }
    }));
  }

  handleSearchButtonClick() {
    this.getFiltered();
  }

  getFiltered() {

    const params = this.getParams();

    fetch(`api/Documents/Get?${params.toString()}`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        return response;
      })
      .then(response => response.json())
      .then(data => {
        this.myRef.current.appendHTML(data.htmlContent);
        this.setState({ isLoading: false, sessionId: data.sessionId, fetchError: false })
      }).catch((error) => {
        this.setState({ fetchError: true})
      });
  }

  getParams() {

    const params = new URLSearchParams();

    const filter = {
      documentNumber: this.state.filter.documentNumber,
      documentName: this.state.filter.documentName,
      date: this.state.filter.date != null ? new Date(this.state.filter.date.getTime()).toLocaleString().replace(/[^ -~]/g, '') : null,
      sessionId: null,
    }
    for (const key in filter) {
      params.set(key, filter[key]);
    }

    return params;
  }

  render() {
    return (
      <div>
        <DocumentsSearchBar
          documentNumber={this.state.filter.documentNumber}
          documentName={this.state.filter.documentName}
          date={this.state.filter.date}
          onDocumentNameChange={this.handleDocumentNameChange}
          onDocumentNumberChange={this.handleDocumentNumberChange}
          onDateChange={this.handleDateChange}
          onSearchButtonClick={this.handleSearchButtonClick}
        />
        <DocumentsViewer fetchError={this.state.fetchError} ref={this.myRef} />
      </div>

    );
  }
}
