import React, { Component } from 'react';
import DatePicker from "react-datepicker";
import { FormGroup, Form, ControlLabel } from 'react-bootstrap';
import "react-datepicker/dist/react-datepicker.css";

export class DocumentsSearchBar extends Component {


  constructor(props) {
    super(props);
    this.handleDocumentNameChange = this.handleDocumentNameChange.bind(this);
    this.handleDocumentNumberChange = this.handleDocumentNumberChange.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.handleButtonClick = this.handleButtonClick.bind(this);
    this.formPreventDefault = this.formPreventDefault.bind(this);
  }

  formPreventDefault(e) {
    e.preventDefault();
  }

  handleDocumentNameChange(e) {
    this.props.onDocumentNameChange(e.target.value);
  }

  handleDocumentNumberChange(e) {
    this.props.onDocumentNumberChange(e.target.value);
  }

  handleDateChange(e) {
    this.props.onDateChange(e);
  }

  handleButtonClick(e) {
    e.preventDefault();
    this.props.onSearchButtonClick();
  }


  render() {
    return (
      <Form inline onSubmit={this.formPreventDefault}>
        <FormGroup>
          <ControlLabel>Document number</ControlLabel>{' '}
          <input type="number"
            className="form-control form-control-sm"
            placeholder="Search by document number"
            value={this.props.documentNumber}
            onChange={this.handleDocumentNumberChange}
          />
        </FormGroup>
        <FormGroup>
          <ControlLabel>Document name</ControlLabel>{' '}
          <input type="text"
            className="form-control form-control-sm"
            placeholder="Search by document name"
            value={this.props.documentName}
            onChange={this.handleDocumentNameChange}
          />
        </FormGroup>
        <FormGroup>
          <ControlLabel>Date</ControlLabel>{' '}
          <DatePicker
            placeholder="Search by date"
            className="form-control"
            selected={this.props.date}
            onChange={this.handleDateChange}
          />
        </FormGroup>
        <button className="btn btn-sm btn-primary"
          onClick={this.handleButtonClick}>
          Search
        </button>
      </Form>














    );
  }
}
