﻿using DocuViewareREST.Models;
using GdPicture14.WEB;
using System;
using System.IO;
using System.Web.Http;

namespace DocuViewareREST.Controllers
{
    public class DocuViewareRESTController : ApiController
    {
        /// <summary>
        /// This POST request will return the control markup that corresponds to the provided session and configuration.
        /// </summary>
        /// <remarks>InitializeDocuVieware has to be called beforehand to make sure the session exists.</remarks>
        /// <param name="controlConfiguration">A DocuViewareConfiguration object</param>
        /// <returns>A DocuViewareRESTOutputResponse JSONS object that contains all the control HTML to include in the client page.</returns>
        [HttpPost]
        [Route("api/DocuViewareREST/GetDocuViewareControl")]
        public DocuViewareRESTOutputResponse GetDocuViewareControl(DocuViewareConfiguration controlConfiguration)
        {
            if (!DocuViewareManager.IsSessionAlive(controlConfiguration.SessionId))
            {
                if (!string.IsNullOrEmpty(controlConfiguration.SessionId) && !string.IsNullOrEmpty(controlConfiguration.ControlId))
                {
                    DocuViewareManager.CreateDocuViewareSession(controlConfiguration.SessionId, controlConfiguration.ControlId, WebApiApplication.SESSION_TIMEOUT);
                }
                else
                {
                    throw new Exception("Invalid session identifier and/or invalid control identifier.");
                }
            }
            DocuVieware docuViewareInstance = new DocuVieware(controlConfiguration.SessionId)
            {
                AllowPrint = controlConfiguration.AllowPrint,
                EnablePrintButton = controlConfiguration.EnablePrintButton,
                AllowUpload = controlConfiguration.AllowUpload,
                EnableFileUploadButton = controlConfiguration.EnableFileUploadButton,
                CollapsedSnapIn = controlConfiguration.CollapsedSnapIn,
                ShowAnnotationsSnapIn = controlConfiguration.ShowAnnotationsSnapIn,
                EnableRotateButtons = controlConfiguration.EnableRotateButtons,
                EnableZoomButtons = controlConfiguration.EnableZoomButtons,
                EnablePageViewButtons = controlConfiguration.EnablePageViewButtons,
                EnableMultipleThumbnailSelection = controlConfiguration.EnableMultipleThumbnailSelection,
                EnableMouseModeButtons = controlConfiguration.EnableMouseModeButtons,
                EnableFormFieldsEdition = controlConfiguration.EnableFormFieldsEdition,
                EnableTwainAcquisitionButton = controlConfiguration.EnableTwainAcquisitionButton,
                MaxUploadSize = 36700160, // 35MB,
                EnableLoadFromUriButton = controlConfiguration.EnableLoadFromUriButton
            };
            using (StringWriter controlOutput = new StringWriter())
            {
                if(controlConfiguration.Document != null && controlConfiguration.Document.Length > 0)
                {
                    docuViewareInstance.LoadFromStream(new MemoryStream(controlConfiguration.Document), true);
                }
                docuViewareInstance.RenderControl(controlOutput);
                DocuViewareRESTOutputResponse output = new DocuViewareRESTOutputResponse
                {
                    HtmlContent = controlOutput.ToString()
                };
                return output;
            }
        }
    }
}
