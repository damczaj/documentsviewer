﻿using Autofac;
using Module = Autofac.Module;
using Autofac.Core;
using MediatR;
using DocumentViewer.Domain.Queries;
using DocumentViewer.Domain.Documents;
using DocumentViewer.Infrastructure.QueryHandlers;
using DocumentViewer.Infrastructure.Context;
using System.Reflection;
using DocumentViewer.Docuvieware3;

namespace DocumentViewer.Bootstrap
{
    public class AppModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<DocumentViewerDbContext>().As<IDocumentViewerDbContext>();

            builder.RegisterType<DbSeeder>();

            builder.RegisterAssemblyTypes(typeof(IMediator).GetTypeInfo().Assembly);

            builder.RegisterType<QueryBus>().As<IQueryBus>();

            builder.RegisterType<DocumentsQueryHandler>().As<IRequestHandler<GetDocument, DocumentItem>>();

            builder.RegisterType<DocuViewareConfigurationFactory>().As<IDocuViewareConfigurationFactory>();
        }

        
    }
}
