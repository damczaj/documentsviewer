﻿using System;

namespace DocumentViewer.Docuvieware3
{
    public interface IDocuViewareConfigurationFactory
    {
        DocuViewareConfiguration GetConfiguration(byte[] document, Guid? sessionId);
    }
}
