﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace DocumentViewer.Docuvieware3
{

    public class DocuviewareService
    {
        public HttpClient Client { get; }
        
        public DocuviewareService(IConfiguration configuration, HttpClient client)
        {
            client.BaseAddress = new Uri(configuration.GetValue<string>("DocuViewareRESTUrl"));
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            
            Client = client;
        }

        public async Task<DocuViewareRESTOutputResponse> GetDocument(DocuViewareConfiguration config)
        {
            if(config == null)
            {
                throw new ArgumentException("Config cannot be null");
            }

            var response = await Client.PostAsync(string.Empty,
                new StringContent(JsonConvert.SerializeObject(config), Encoding.UTF8, "application/json"));

            if(!response.IsSuccessStatusCode)
            {
                throw new Exception($"The request return following code: {response.StatusCode.ToString()}. {response.Content}");
            }

            var controlHtml = await response.Content.ReadAsStringAsync();

            var markup = (DocuViewareRESTOutputResponse)JsonConvert
                .DeserializeObject(controlHtml, typeof(DocuViewareRESTOutputResponse));

            markup.SessionId = config.SessionId;

            return markup;

        } 
    }
}
