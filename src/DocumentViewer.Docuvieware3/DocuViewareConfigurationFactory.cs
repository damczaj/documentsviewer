﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocumentViewer.Docuvieware3
{

    public class DocuViewareConfigurationFactory : IDocuViewareConfigurationFactory
    {
        public DocuViewareConfiguration GetConfiguration(byte[] document, Guid? sessionId)
        {
            return new DocuViewareConfiguration()
            {
                ControlId = "DocuVieware1",
                SessionId = sessionId.HasValue ? sessionId.ToString() : Guid.NewGuid().ToString(),                
                AllowPrint = false,
                EnablePrintButton = false,
                AllowUpload = false,
                EnableFileUploadButton = false,
                CollapsedSnapIn = false,
                ShowAnnotationsSnapIn = false,
                EnableRotateButtons = false,
                EnableZoomButtons = false,
                EnablePageViewButtons = false,
                EnableMultipleThumbnailSelection = false,
                EnableMouseModeButtons = false,
                EnableFormFieldsEdition = false,
                EnableTwainAcquisitionButton = false,
                EnableLoadFromUriButton = false,
                Document = document
            };
        }
    }
}
