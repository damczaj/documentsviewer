﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocumentViewer.Docuvieware3
{
    public class DocuViewareRESTOutputResponse
    {
        public string HtmlContent;
        public string SessionId;
    }
}
