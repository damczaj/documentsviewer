﻿using MediatR;

namespace DocumentViewer.Domain.Queries
{
    public interface IQuery<out TResponse> : IRequest<TResponse>
    {
    }
}
