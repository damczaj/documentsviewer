﻿using System;

namespace DocumentViewer.Domain.Documents
{
    public class DocumentItem
    {
        public DateTime Date { get; set; }
        public string DocumentName { get; set; }
        public long DocumentNumber { get; set; }
        public byte[] Document { get; set; }
    }
}
