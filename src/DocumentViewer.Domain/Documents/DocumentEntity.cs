﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocumentViewer.Domain.Documents
{
    public class DocumentEntity
    {
        public DocumentEntity(string documentName, byte[] document, DateTime date, long documentNumber)
        {
            DocumentName = documentName;
            Document = document;
            DateTime = date;
            DocumentNumber = documentNumber;
        }
        protected DocumentEntity()
        {

        }

        public long Id { get; protected set; }
        public DateTime DateTime { get; protected set; }
        public string DocumentName { get; protected set; }
        public long DocumentNumber { get; protected set; }
        public byte[] Document { get; protected set; }
    }
}
