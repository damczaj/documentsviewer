﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocumentViewer.Domain.Documents
{
    public class DocumentQueryItem
    {
        public DateTime? Date { get; set; }
        public string DocumentName { get; set; }
        public int? DocumentNumber { get; set; }
        public Guid? SessionId { get; set; }
    }
}
