﻿using DocumentViewer.Domain.Queries;
using System;

namespace DocumentViewer.Domain.Documents
{
    public class GetDocument : IQuery<DocumentItem>
    {
        public DateTime? Date { get;  }
        public string DocumentName { get; }
        public int? DocumentNumber { get; }

        public GetDocument(DateTime? date, string documentName, int? documentNumber)
        {
            Date = date;
            DocumentName = documentName;
            DocumentNumber = documentNumber;
        }
    }
}
