﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocumentViewer.Infrastructure.Extensions
{
    public static class StringExtensions
    {
        public static bool ContainsInvariantCultureIgnoreCase(this string from, string value)
        {
            if (string.IsNullOrWhiteSpace(from) || string.IsNullOrWhiteSpace(value))
            {
                return false;
            }

            return from.IndexOf(value, StringComparison.InvariantCultureIgnoreCase) >= 0;
        }
    }
}
