﻿using DocumentViewer.Domain.Documents;
using DocumentViewer.Domain.Queries;
using DocumentViewer.Infrastructure.Context;
using DocumentViewer.Infrastructure.Extensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DocumentViewer.Infrastructure.QueryHandlers
{
    public class DocumentsQueryHandler : IQueryHandler<GetDocument, DocumentItem>
    {
        private IQueryable<DocumentEntity> Documents;
        public DocumentsQueryHandler(IDocumentViewerDbContext dbContext)
        {
            Documents = dbContext.Documents;
        }    

        public Task<DocumentItem> Handle(GetDocument request, CancellationToken cancellationToken = default(CancellationToken))
        {
            var documents = Documents;
            documents = request.DocumentNumber.HasValue 
                ? documents.Where(x => x.DocumentNumber == request.DocumentNumber) 
                : documents;
            documents = request.Date.HasValue 
                ? documents.Where(x => x.DateTime.Date == request.Date.Value) 
                : documents;
            documents = !string.IsNullOrWhiteSpace(request.DocumentName) 
                ? documents.Where(x => x.DocumentName.ContainsInvariantCultureIgnoreCase(request.DocumentName)) 
                : documents;

            var document = documents
                .Select(d => new DocumentItem()
                {
                    Document = d.Document,
                    Date = d.DateTime,
                    DocumentName = d.DocumentName,
                    DocumentNumber = d.DocumentNumber
                })
                .FirstOrDefault();

            var result = document ?? new DocumentItem() { DocumentNumber = 0, Document = new byte[0], Date = DateTime.MinValue };

            return Task.FromResult(result);
        }
    }
}
