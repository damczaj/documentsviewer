﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocumentViewer.Infrastructure.Context
{
    public class DbSeeder
    {
        private readonly DocumentViewerDbContext _dbContext;
        private readonly IConfiguration _configuration;

        public DbSeeder(DocumentViewerDbContext documentViewerDbContext, IConfiguration configuration)
        {
            _dbContext = documentViewerDbContext;
            _configuration = configuration;
        }

        public void Seed()
        {
            if (_dbContext.Database.GetPendingMigrations().Any())
                _dbContext.Database.Migrate();
        }
    }
}
