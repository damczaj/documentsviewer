﻿using DocumentViewer.Domain.Documents;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocumentViewer.Infrastructure.Context
{
    public interface IDocumentViewerDbContext
    {
        DbSet<DocumentEntity> Documents { get; set; }
    }
}
