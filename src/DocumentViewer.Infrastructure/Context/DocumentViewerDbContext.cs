﻿using DocumentViewer.Domain.Documents;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocumentViewer.Infrastructure.Context
{
    public class DocumentViewerDbContext : DbContext, IDocumentViewerDbContext
    {
        public DocumentViewerDbContext(DbContextOptions options) : base(options)
        {
        }

        protected DocumentViewerDbContext()
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            EntityMappings.RegisterMaps(modelBuilder);
        }

        public DbSet<DocumentEntity> Documents { get; set; }
    }
}
