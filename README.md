# DocumentsViewer


## Requirements to run:

- Node.js
- .NET Core 2.1
- .NET Framework 4.5

## How to run

1. Get license key of Docuvieware. 
2  Go to src/DocuViewareREST/web.config and replace the license key variable "DocuviewareLicenseKey" in <appSetting>.
3. Run the web application called "DocuViewareREST"
4. Go to /src/DocumentViewer.WebApi/appSettings.json
5. Replace the connection string to Documents Database 
6. Replace the "DocuViewareRESTUrl" variable to URL of application from point 3. 
   After replacement it should looks like: <YOUR_OURL>/api/DocuViewareREST/GetDocuViewareControl
   


